package com.codingtask.simplebanking;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codingtask.simplebanking.domain.Client;

@Service
public class AuthenticationService {

    private ClientRepository clientRepository;

    @Autowired
    public AuthenticationService(ClientRepository clientRepository) {
        this.clientRepository = clientRepository;
    }

    public Optional<Client> findClient(String email, String password) {
        return clientRepository.findClientByEmailAndPassword(email, password);
    }
}
