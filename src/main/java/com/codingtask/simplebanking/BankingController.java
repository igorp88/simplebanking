package com.codingtask.simplebanking;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.codingtask.simplebanking.domain.Client;
import com.codingtask.simplebanking.domain.Transaction;

@RestController
public class BankingController {

    private ClientRepository clientRepository;
    private AuthenticationService authenticationService;
    private SimpleBankingService simpleBankingService;

    @Autowired
    BankingController(ClientRepository clientRepository,
                      AuthenticationService authenticationService,
                      SimpleBankingService simpleBankingService) {
        this.clientRepository = clientRepository;
        this.authenticationService = authenticationService;
        this.simpleBankingService = simpleBankingService;
    }

    // imitation of login operation
    @GetMapping(path = "/simplebank/client")
    public ResponseEntity<Client> findClient(
            @RequestParam(name="email", defaultValue="Client1@simplebank.com") String email,
            @RequestParam(name="password", defaultValue="passClient1") String password){
        Optional<Client> client = authenticationService.findClient(email, password);
        return client.isPresent() ? new ResponseEntity<>( HttpStatus.OK) : new ResponseEntity<>(HttpStatus.NOT_FOUND);
    }

    @GetMapping(path = "/simplebank/deposit/{clientName}/{depositAmount}")
    public ResponseEntity<Void> depositOperation(@PathVariable String clientName,
                                                 @PathVariable String depositAmount){
        simpleBankingService.deposit(clientName, new BigDecimal(depositAmount));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/simplebank/withdraw/{clientName}/{withdrawAmount}")
    public ResponseEntity<Void> withdrawOperation(@PathVariable String clientName,
                                                  @PathVariable String withdrawAmount){
        simpleBankingService.withdraw(clientName, new BigDecimal(withdrawAmount));
        return new ResponseEntity<>(HttpStatus.OK);
    }

    @GetMapping("/simplebank/balance/{clientName}")
    public BigDecimal clientBalance(@PathVariable String clientName){
        return simpleBankingService.getClientBalance(clientName);
    }

    @GetMapping("/simplebank/statement/{clientName}")
    public List<Transaction> clientStatement(@PathVariable String clientName){
        return simpleBankingService.getClientStatement(clientName);
    }

    @GetMapping("/clients")
    List<Client> getAllClients() {
        return this.clientRepository.findAll();
    }
}
