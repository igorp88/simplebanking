package com.codingtask.simplebanking;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.codingtask.simplebanking.domain.Client;

@Repository
public interface ClientRepository extends JpaRepository<Client, Long> {
    Optional<Client> findClientByEmailAndPassword(String email, String password);
    Optional<Client> findClientByName(String clientName);
}