package com.codingtask.simplebanking;

import java.math.BigDecimal;
import java.util.List;
import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.codingtask.simplebanking.domain.Client;
import com.codingtask.simplebanking.domain.Transaction;

@Service
@Transactional
public class SimpleBankingService {

    @Autowired
    private ClientRepository clientRepository;
    @Autowired
    private TransactionRepository transactionRepository;

    private void createTransaction(String clientName, BigDecimal transactionAmount, TransactionType transactionType) {
        transactionRepository.save(new Transaction(clientName, transactionAmount, transactionType));
    }

    public void deposit(String clientName, BigDecimal depositAmount) {
        Optional<Client> client = getClientByName(clientName);
        if (client.isPresent()) {
            Client cl = client.get();
            BigDecimal actualBalance = cl.getBalance();
            cl.setBalance(actualBalance.add(depositAmount));
            createTransaction(clientName, depositAmount, TransactionType.DEPOSIT);
        }
    }

    public void withdraw(String clientName, BigDecimal withdrawAmount) {
        Optional<Client> client = getClientByName(clientName);
        if (client.isPresent()) {
            Client cl = client.get();
            BigDecimal actualBalance = cl.getBalance();
            cl.setBalance(actualBalance.subtract(withdrawAmount));
            createTransaction(clientName, withdrawAmount, TransactionType.WITHDRAW);
        }
    }

    public BigDecimal getClientBalance(String clientName) {
        Optional<Client> client = getClientByName(clientName);
        if (client.isPresent()) {
            return client.get().getBalance();
        } else {
            return BigDecimal.ZERO;
        }
    }

    public List<Transaction> getClientStatement(String clientName) {
        return transactionRepository.findTransactionsByClientName(clientName);
    }

    private Optional<Client> getClientByName(String clientName) {
        return clientRepository.findClientByName(clientName);
    }

}
