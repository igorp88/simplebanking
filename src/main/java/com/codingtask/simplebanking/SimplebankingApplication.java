package com.codingtask.simplebanking;

import java.util.stream.Stream;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;

import com.codingtask.simplebanking.domain.Client;

@SpringBootApplication
@EntityScan( basePackages = {"com.codingtask.simplebanking"} )
public class SimplebankingApplication {

    public static void main(String[] args) {
        SpringApplication.run(SimplebankingApplication.class, args);
    }

    @Bean
    ApplicationRunner run(ClientRepository cr) {

        return args -> Stream.of("Client1", "Client2")
                .forEach(client -> {
                    Client cl = new Client(client, "pass"
                            + client, client
                            + "@simplebank.com");

                    cr.save(cl);
                });
    }
}

