package com.codingtask.simplebanking;

public enum TransactionType {

        DEPOSIT("D", "Client increases the amount of money"),
        WITHDRAW("W", "Client reduces the amount of money");

        private final String value;
        private final String description;

        TransactionType(String value, String description) {
            this.value = value;
            this.description = description;
        }

        public String getDescription() {
            return description;
        }

        public String getValue() {
            return value;
        }
}
