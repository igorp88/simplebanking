package com.codingtask.simplebanking.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Client {

    @Id
    @GeneratedValue
    private Long id;
    private String name;
    private String password;
    private String email;
    private BigDecimal balance;

    public Client(String client, String password, String email) {
        this.name = client;
        this.password = password;
        this.email = email;
        this.balance = BigDecimal.ZERO;
    }

    public void setBalance(BigDecimal balance) {
        this.balance = balance;
    }

    public BigDecimal getBalance() {
        return this.balance;
    }
}
