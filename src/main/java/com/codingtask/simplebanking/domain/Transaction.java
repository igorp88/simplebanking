package com.codingtask.simplebanking.domain;

import java.math.BigDecimal;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;

import com.codingtask.simplebanking.TransactionType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Transaction {

    @Id
    @GeneratedValue
    private Long transactionId;
    private String clientName;
    private BigDecimal transactionAmount;
    private TransactionType transactionType;

    public Transaction(String clientName, BigDecimal transactionAmount, TransactionType transactionType) {
        this.clientName = clientName;
        this.transactionAmount = transactionAmount;
        this.transactionType = transactionType;
    }
}
