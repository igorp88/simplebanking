package com.codingtask.simplebanking;

import static org.mockito.Matchers.isA;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.math.BigDecimal;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.json.JacksonTester;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.codingtask.simplebanking.domain.Client;
import com.fasterxml.jackson.databind.ObjectMapper;

@RunWith(SpringRunner.class)
@SpringBootTest
@AutoConfigureMockMvc
public class BankingControllerTest {

    private Client rightClient;

    @Autowired
    private ObjectMapper objectMapper;
    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ClientRepository clientRepository;
    @MockBean
    private AuthenticationService authenticationService;
    @MockBean
    private SimpleBankingService simpleBankingService;

    @Before
    public void setUp() {
        rightClient = new Client("Client1", "passClient1", "Client1@simplebank.com");
        clientRepository.save(rightClient);
        JacksonTester.initFields(this, objectMapper);
    }

    @Test
    public void findClient() throws Exception {
        when(authenticationService.findClient("Client1@simplebank.com", "passClient1"))
                .thenReturn(java.util.Optional.ofNullable(rightClient));
        mockMvc.perform(get("/simplebank/client")
                .content(String.valueOf(rightClient)))
                .andExpect(status().isOk());
    }

    @Test
    public void depositOperation() throws Exception {
        doNothing().when(simpleBankingService).deposit(isA(String.class), isA(BigDecimal.class));
        mockMvc.perform(get("/simplebank/deposit/Client1/88"))
                .andExpect(status().isOk());
    }

    @Test
    public void withdrawOperation() throws Exception {
        doNothing().when(simpleBankingService).deposit(isA(String.class), isA(BigDecimal.class));
        mockMvc.perform(get("/simplebank/withdraw/Client1/123"))
                .andExpect(status().isOk());
    }

    @Test
    public void clientBalance() {
    }

    @Test
    public void clientStatement() {
    }

    @Test
    public void getAllClients() {
    }
}